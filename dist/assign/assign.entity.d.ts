import { UserEntity } from '../users/user.entity';
import { CarEntity } from '../cars/car.entity';
export declare class AssignEntity {
    id: number;
    userId: number;
    carId: number;
    user: UserEntity;
    car: CarEntity;
}
