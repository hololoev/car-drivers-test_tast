"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const assign_service_1 = require("./assign.service");
const assign_resolver_1 = require("./assign.resolver");
const assign_entity_1 = require("./assign.entity");
const car_entity_1 = require("../cars/car.entity");
const user_entity_1 = require("../users/user.entity");
let AssignModule = class AssignModule {
};
AssignModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([assign_entity_1.AssignEntity, car_entity_1.CarEntity, user_entity_1.UserEntity])],
        providers: [assign_service_1.AssignService, assign_resolver_1.AssignResolver],
        exports: [typeorm_1.TypeOrmModule]
    })
], AssignModule);
exports.AssignModule = AssignModule;
//# sourceMappingURL=assign.module.js.map