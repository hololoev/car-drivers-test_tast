import { AssignEntity } from './assign.entity';
import { AssignService } from './assign.service';
import { AssignDto } from './dto/assign.args';
export declare class AssignResolver {
    private assignService;
    constructor(assignService: AssignService);
    getAssigns(limit: number, offset: number): Promise<AssignEntity[]>;
    getUser(id: number): Promise<AssignEntity>;
    createUser(args: AssignDto): Promise<AssignEntity>;
}
