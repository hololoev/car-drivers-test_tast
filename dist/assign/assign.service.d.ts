import { Repository } from 'typeorm';
import { AssignEntity } from './assign.entity';
import { CarEntity } from '../cars/car.entity';
import { UserEntity } from '../users/user.entity';
export declare class AssignService {
    private assignRepository;
    private carRepository;
    private userRepository;
    constructor(assignRepository: Repository<AssignEntity>, carRepository: Repository<CarEntity>, userRepository: Repository<UserEntity>);
    getAll(limit: number, offset: number, term?: string): Promise<AssignEntity[]>;
    getById(id: number): Promise<AssignEntity>;
    create(assing: any): Promise<AssignEntity>;
}
