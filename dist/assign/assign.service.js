"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const assign_entity_1 = require("./assign.entity");
const car_entity_1 = require("../cars/car.entity");
const user_entity_1 = require("../users/user.entity");
let AssignService = class AssignService {
    constructor(assignRepository, carRepository, userRepository) {
        this.assignRepository = assignRepository;
        this.carRepository = carRepository;
        this.userRepository = userRepository;
    }
    async getAll(limit, offset, term = '') {
        let assigns = await this.assignRepository.find({
            take: limit,
            skip: offset
        });
        let userIDs = [];
        let carIDs = [];
        for (let assign of assigns) {
            userIDs.push(assign.userId);
            carIDs.push(assign.carId);
        }
        let users = await this.userRepository.find({ where: { id: (0, typeorm_2.In)(userIDs) } });
        let cars = await this.carRepository.find({ where: { id: (0, typeorm_2.In)(carIDs) } });
        for (let assign of assigns) {
            assign.user = users.find(user => user.id === assign.userId);
            assign.car = cars.find(car => car.id === assign.carId);
        }
        return assigns;
    }
    async getById(id) {
        let assign = await this.assignRepository.findOneBy({ id });
        assign.user = await this.userRepository.findOneBy({ id: assign.userId });
        assign.car = await this.carRepository.findOneBy({ id: assign.carId });
        return assign;
    }
    async create(assing) {
        let userExists = await this.userRepository.findOneBy({ id: assing.userId });
        if (!userExists)
            throw new Error('User not found');
        let carExists = await this.carRepository.findOneBy({ id: assing.carId });
        if (!carExists)
            throw new Error('Car not found');
        let assignExists = await this.assignRepository.findOneBy(Object.assign({}, assing));
        if (assignExists)
            throw new Error('Assignation already exists');
        let newAssign = await this.assignRepository.save(Object.assign({}, assing));
        return this.getById(newAssign.id);
    }
};
AssignService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(assign_entity_1.AssignEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(car_entity_1.CarEntity)),
    __param(2, (0, typeorm_1.InjectRepository)(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], AssignService);
exports.AssignService = AssignService;
//# sourceMappingURL=assign.service.js.map