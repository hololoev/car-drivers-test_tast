import { Repository } from 'typeorm';
import { UsageEntity } from './usage.entity';
export declare class UsageService {
    private usageRepository;
    constructor(usageRepository: Repository<UsageEntity>);
    getAll(limit: number, offset: number): Promise<UsageEntity[]>;
    getById(id: number): Promise<UsageEntity>;
    create(usage: any): Promise<UsageEntity>;
}
