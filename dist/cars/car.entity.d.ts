export declare class CarEntity {
    id: number;
    producer: string;
    name: string;
    year: number;
}
