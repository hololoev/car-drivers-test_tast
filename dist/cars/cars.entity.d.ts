export declare class CarsEntity {
    id: number;
    producer: string;
    name: string;
    year: number;
}
