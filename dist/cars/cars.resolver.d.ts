import { CarEntity } from './car.entity';
import { CarsService } from './cars.service';
import { CarDto } from './dto/car.args';
export declare class CarsResolver {
    private carService;
    constructor(carService: CarsService);
    getCars(limit: number, offset: number): Promise<CarEntity[]>;
    getCar(id: number): Promise<CarEntity>;
    createCar(args: CarDto): Promise<CarEntity>;
}
