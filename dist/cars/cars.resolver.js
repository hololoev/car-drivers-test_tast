"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarsResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const car_entity_1 = require("./car.entity");
const cars_service_1 = require("./cars.service");
const car_args_1 = require("./dto/car.args");
let CarsResolver = class CarsResolver {
    constructor(carService) {
        this.carService = carService;
    }
    async getCars(limit, offset) {
        return this.carService.getAll(limit, offset);
    }
    async getCar(id) {
        return this.carService.getById(id);
    }
    async createCar(args) {
        return this.carService.create(Object.assign({}, args));
    }
};
__decorate([
    (0, graphql_1.Query)(returns => [car_entity_1.CarEntity], {
        name: 'getCars',
        description: 'Get car list',
    }),
    __param(0, (0, graphql_1.Args)('limit', { type: () => graphql_1.Int })),
    __param(1, (0, graphql_1.Args)('offset', { type: () => graphql_1.Int })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], CarsResolver.prototype, "getCars", null);
__decorate([
    (0, graphql_1.Query)(returns => car_entity_1.CarEntity, {
        name: 'getCar',
        description: 'Get car by id',
    }),
    __param(0, (0, graphql_1.Args)('id', { type: () => graphql_1.Int })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CarsResolver.prototype, "getCar", null);
__decorate([
    (0, graphql_1.Mutation)(returns => car_entity_1.CarEntity, {
        name: 'createCar',
        description: 'Create new car',
    }),
    __param(0, (0, graphql_1.Args)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [car_args_1.CarDto]),
    __metadata("design:returntype", Promise)
], CarsResolver.prototype, "createCar", null);
CarsResolver = __decorate([
    (0, graphql_1.Resolver)(),
    __metadata("design:paramtypes", [cars_service_1.CarsService])
], CarsResolver);
exports.CarsResolver = CarsResolver;
//# sourceMappingURL=cars.resolver.js.map