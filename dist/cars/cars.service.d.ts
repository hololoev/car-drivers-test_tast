import { Repository } from 'typeorm';
import { CarEntity } from './car.entity';
export declare class CarsService {
    private carsRepository;
    constructor(carsRepository: Repository<CarEntity>);
    getAll(limit: number, offset: number): Promise<CarEntity[]>;
    getById(id: number): Promise<CarEntity>;
    create(car: any): Promise<CarEntity>;
}
