export declare class CarDto {
    name: string;
    producer: string;
    year: number;
}
