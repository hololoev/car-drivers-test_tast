import { AssignService } from './assign.service';
export declare class AssignResolver {
    private assignService;
    constructor(assignService: AssignService);
    getAssigns(limit: number, offset: number): Promise<any>;
}
