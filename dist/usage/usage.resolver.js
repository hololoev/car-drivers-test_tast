"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const assign_service_1 = require("./assign.service");
let AssignResolver = class AssignResolver {
    constructor(assignService) {
        this.assignService = assignService;
    }
    async getAssigns(limit, offset) {
        return this.AssignService.getAll(limit, offset);
    }
};
__decorate([
    Query(returns => [UserEntity], {
        name: 'getAssigns',
        description: 'Get user/car usage',
    }),
    __param(0, Args('limit', { type: () => Int })),
    __param(1, Args('offset', { type: () => Int })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], AssignResolver.prototype, "getAssigns", null);
AssignResolver = __decorate([
    (0, graphql_1.Resolver)(),
    __metadata("design:paramtypes", [typeof (_a = typeof assign_service_1.AssignService !== "undefined" && assign_service_1.AssignService) === "function" ? _a : Object])
], AssignResolver);
exports.AssignResolver = AssignResolver;
//# sourceMappingURL=usage.resolver.js.map