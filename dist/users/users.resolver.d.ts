import { UserEntity } from './user.entity';
import { UsersService } from './users.service';
import { UserDto } from './dto/user.args';
export declare class UsersResolver {
    private userService;
    constructor(userService: UsersService);
    getUsers(limit: number, offset: number): Promise<UserEntity[]>;
    getUser(id: number): Promise<UserEntity>;
    createUser(args: UserDto): Promise<UserEntity>;
}
