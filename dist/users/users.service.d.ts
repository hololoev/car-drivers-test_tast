import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
export declare class UsersService {
    private usersRepository;
    constructor(usersRepository: Repository<UserEntity>);
    getAll(limit: number, offset: number): Promise<UserEntity[]>;
    getById(id: number): Promise<UserEntity>;
    create(user: any): Promise<UserEntity>;
}
