#!/bin/sh
cd /server
npm i
npm i -g pm2
npx nest build
pm2 start ./dist/main.js && pm2 logs
