import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';

import { AppService } from './app.service';
import { AppController } from './app.controller';

import { UsersModule } from './users/users.module';
import { CarsModule } from './cars/cars.module';
import { AssignModule } from './assign/assign.module';

import { UserEntity } from './users/user.entity';
import { CarEntity } from './cars/car.entity';
import { AssignEntity } from './assign/assign.entity';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
      debug: false,
      playground: true,
      sortSchema: true
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_HOST,
      port: parseInt(process.env.MYSQL_PORT),
      username: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE,
      entities: [ UserEntity, CarEntity, AssignEntity ],
      synchronize: true,
      logging: true
    }),
    UsersModule,
    CarsModule,
    AssignModule,
  ],
  controllers: [ AppController ],
  providers: [ AppService ],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
