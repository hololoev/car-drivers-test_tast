// TODO use correct nested entities
import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';

import { UserEntity } from '../users/user.entity';
import { CarEntity } from '../cars/car.entity';

@Entity('assign')
@ObjectType()
export class AssignEntity {
  @Field(type => Int)
  @PrimaryGeneratedColumn()
  id: number;

//   @OneToOne(() => UserEntity)
//   @JoinColumn({ name: "userId" })
  @Field()
  @Column()
  userId: number;

//   @OneToOne(() => CarEntity)
//   @JoinColumn({ name: "carId" })
  @Field()
  @Column()
  carId: number;

  @Field()
  user: UserEntity

  @Field()
  car: CarEntity
}
