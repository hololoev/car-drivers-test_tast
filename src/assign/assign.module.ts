import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AssignService } from './assign.service';
import { AssignResolver } from './assign.resolver';
import { AssignEntity } from './assign.entity';

import { CarEntity } from '../cars/car.entity';
import { UserEntity } from '../users/user.entity';

@Module({
  imports: [ TypeOrmModule.forFeature([ AssignEntity, CarEntity, UserEntity ]) ],
  providers: [ AssignService, AssignResolver ],
  exports: [ TypeOrmModule ]
})
export class AssignModule {}
