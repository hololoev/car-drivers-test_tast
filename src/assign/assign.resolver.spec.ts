import { Test, TestingModule } from '@nestjs/testing';
import { AssignResolver } from './assign.resolver';

describe('AssignResolver', () => {
  let resolver: UsageResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AssignResolver],
    }).compile();

    resolver = module.get<AssignResolver>(AssignResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
