import { Resolver, Query, Mutation, Int, Args } from '@nestjs/graphql';

import { AssignEntity } from './assign.entity';
import { AssignService } from './assign.service';
import { AssignDto } from './dto/assign.args';

//import { CarsService } from '../cars/cars.service';

@Resolver()
export class AssignResolver {
  constructor(
    private assignService: AssignService
    //private carsService: CarsService
  ) {}

  @Query(returns => [ AssignEntity ], {
    name: 'getAssigns',
    description: 'Get user/car assignations',
  })
  async getAssigns(
    @Args('limit', { type: () => Int }) limit: number,
    @Args('offset', { type: () => Int }) offset: number
  ) {
    return this.assignService.getAll(limit, offset);
  }

  @Query(returns => AssignEntity, {
    name: 'getAssign',
    description: 'Get user/car assignations by id',
  })
  async getUser(@Args('id', { type: () => Int }) id: number) {
    return this.assignService.getById(id);
  }

  @Mutation(returns => AssignEntity, {
    name: 'createAssign',
    description: 'Create new user/car assignation',
  })
  async createUser(
    @Args() args: AssignDto
  ) {
    return this.assignService.create({ ...args });
  }
}
