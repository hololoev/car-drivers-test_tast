import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';

import { AssignEntity } from './assign.entity';
import { CarsService } from '../cars/cars.service';

import { CarEntity } from '../cars/car.entity';
import { UserEntity } from '../users/user.entity';

@Injectable()
export class AssignService {
  constructor(
    @InjectRepository(AssignEntity)
    private assignRepository: Repository<AssignEntity>,

    @InjectRepository(CarEntity)
    private carRepository: Repository<CarEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}
  
  async getAll(limit: number, offset: number, term: string = ''): Promise<AssignEntity[]> {
    // TODO use correct relations
    let assigns: AssignEntity[]  = await this.assignRepository.find({
      take: limit,
      skip: offset
    });

    let userIDs: number[] = [];
    let carIDs: number[] = [];

    for(let assign of assigns) {
      userIDs.push(assign.userId);
      carIDs.push(assign.carId);
    }

    let users = await this.userRepository.find({ where: { id: In(userIDs) } });
    let cars = await this.carRepository.find({ where: { id: In(carIDs) } });

    for(let assign of assigns) {
      assign.user = users.find( user => user.id === assign.userId );
      assign.car = cars.find( car => car.id === assign.carId );
    }

    return assigns;
  }

  async getById(id: number): Promise<AssignEntity>  {
    // TODO use correct relations
    let assign: AssignEntity = await this.assignRepository.findOneBy({ id });
    assign.user = await this.userRepository.findOneBy({ id: assign.userId });
    assign.car = await this.carRepository.findOneBy({ id: assign.carId });
    
    return assign
  }

  async create(assing: any): Promise<AssignEntity> {
    // TODO use correct relations

    let userExists = await this.userRepository.findOneBy({ id: assing.userId });
    if( !userExists ) throw new Error('User not found');

    let carExists = await this.carRepository.findOneBy({ id: assing.carId });
    if( !carExists ) throw new Error('Car not found');

    // TODO use constraint instead
    let assignExists = await this.assignRepository.findOneBy({ ...assing });
    if( assignExists ) throw new Error('Assignation already exists');
    
    let newAssign = await this.assignRepository.save({ ...assing });

    return this.getById(newAssign.id);
  }
}
