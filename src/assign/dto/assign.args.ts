import { MinLength, MaxLength } from 'class-validator';
import { Field, ArgsType } from '@nestjs/graphql';

@ArgsType()
export class AssignDto {
  @Field()
  userId: number;

  @Field()
  carId: number;
}
