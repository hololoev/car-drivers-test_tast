import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('cars')
@ObjectType()
export class CarEntity {
  @Field(type => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ length: 256 })
  producer: string;

  @Field()
  @Column({ length: 256 })
  name: string;

  @Field(type => Int)
  @Column()
  year: number;
}
