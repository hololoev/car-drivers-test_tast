import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CarsResolver } from './cars.resolver';
import { CarsService } from './cars.service';
import { CarEntity } from './car.entity';

@Module({
  imports: [ TypeOrmModule.forFeature([ CarEntity ]) ],
  providers: [ CarsResolver, CarsService ],
  exports: [ TypeOrmModule ]
})
export class CarsModule {}
