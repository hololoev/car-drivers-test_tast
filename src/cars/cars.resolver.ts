import { Resolver, Query, Mutation, Int, Args } from '@nestjs/graphql';

import { CarEntity } from './car.entity';
import { CarsService } from './cars.service';
import { CarDto } from './dto/car.args';

@Resolver()
export class CarsResolver {
  constructor(
    private carService: CarsService
  ) {}

  @Query(returns => [ CarEntity ], {
    name: 'getCars',
    description: 'Get car list',
  })
  async getCars(
    @Args('limit', { type: () => Int }) limit: number,
    @Args('offset', { type: () => Int }) offset: number
  ) {
    return this.carService.getAll(limit, offset);
  }

  @Query(returns => CarEntity, {
    name: 'getCar',
    description: 'Get car by id',
  })
  async getCar(@Args('id', { type: () => Int }) id: number) {
    return this.carService.getById(id);
  }

  @Mutation(returns => CarEntity, {
    name: 'createCar',
    description: 'Create new car',
  })
  async createCar(
    @Args() args: CarDto
  ) {
    return this.carService.create({ ...args });
  }
}
