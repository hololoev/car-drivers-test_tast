import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CarEntity } from './car.entity';

@Injectable()
export class CarsService {
  constructor(
    @InjectRepository(CarEntity)
    private carsRepository: Repository<CarEntity>
  ) {}

  async getAll(limit: number, offset: number): Promise<CarEntity[]> {
    return await this.carsRepository.find({
      take: limit,
      skip: offset
    });
  }

  async getById(id: number): Promise<CarEntity>  {
    return this.carsRepository.findOneBy({ id });
  }

  async create(car: any): Promise<CarEntity> {
    return await this.carsRepository.save({ ...car });
  }
}
