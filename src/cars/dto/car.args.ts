import { MinLength, MaxLength } from 'class-validator';
import { Field, ArgsType } from '@nestjs/graphql';

@ArgsType()
export class CarDto {
  @Field({ nullable: false })
  @MinLength(2)
  @MaxLength(255)
  name: string;

  @Field({ nullable: false })
  @MinLength(2)
  @MaxLength(255)
  producer: string;

  @Field({ nullable: false })
  year: number;
}
