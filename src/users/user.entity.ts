import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
@ObjectType()
export class UserEntity {
  @Field(type => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ length: 256 })
  name: string;

  @Field()
  @Column({ length: 256 })
  email: string;
}
