import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { UserEntity } from './user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ UserEntity ])],
  providers: [ UsersService, UsersResolver ],
  exports: [ TypeOrmModule ]
})
export class UsersModule {}
