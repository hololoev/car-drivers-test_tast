import { Resolver, Query, Mutation, Int, Args } from '@nestjs/graphql';

import { UserEntity } from './user.entity';
import { UsersService } from './users.service';
import { UserDto } from './dto/user.args';

@Resolver()
export class UsersResolver {
  constructor(
    private userService: UsersService
  ) {}

  @Query(returns => [ UserEntity ], {
    name: 'getUsers',
    description: 'Get user list',
  })
  async getUsers(
    @Args('limit', { type: () => Int }) limit: number,
    @Args('offset', { type: () => Int }) offset: number
  ) {
    return this.userService.getAll(limit, offset);
  }

  @Query(returns => UserEntity, {
    name: 'getUser',
    description: 'Get user by id',
  })
  async getUser(@Args('id', { type: () => Int }) id: number) {
    return this.userService.getById(id);
  }

  @Mutation(returns => UserEntity, {
    name: 'createUser',
    description: 'Create new user',
  })
  async createUser(
    @Args() args: UserDto
  ) {
    return this.userService.create({ ...args });
  }
}
