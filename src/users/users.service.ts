import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserEntity } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>
  ) {}

  async getAll(limit: number, offset: number): Promise<UserEntity[]> {
    return await this.usersRepository.find({
      take: limit,
      skip: offset
    });
  }

  async getById(id: number): Promise<UserEntity>  {
    return this.usersRepository.findOneBy({ id });
  }

  async create(user: any): Promise<UserEntity> {
    return await this.usersRepository.save({ ...user });
  }
}
